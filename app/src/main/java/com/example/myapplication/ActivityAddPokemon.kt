package com.example.myapplication

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import kotlinx.android.synthetic.main.activity_add_pokemon.*

class ActivityAddPokemon : AppCompatActivity(){

    private lateinit var place: Place

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_pokemon)
        setUpLocationInputClick()
    }

    private fun setUpLocationInputClick(){
        editLocalizationInput.setOnClickListener{
            val fields = listOf(Place.Field.ADDRESS, Place.Field.LAT_LNG)
            val intent =
                Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this)
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
          if(requestCode == AUTOCOMPLETE_REQUEST_CODE && data != null){
              if(resultCode == Activity.RESULT_OK){
                   place = Autocomplete.getPlaceFromIntent(data)
                  editLocalizationInput.setText(place.address)

              }
          }
    }
    companion object{
        const val AUTOCOMPLETE_REQUEST_CODE = 1
    }
}