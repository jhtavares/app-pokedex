package com.example.myapplication

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.libraries.places.api.Places
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.list_item_pokemon.view.*

class MainActivity : AppCompatActivity() {

     private var pokemons: List<Pokemon> = listOf(
            Pokemon(
                "Pikachu",
                25,
                listOf("Eletric"),
                "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png",
               5f,
                4f,
                -3.1020263,
                -60.8147652

            ),
            Pokemon(
                "Squirtle",
                7,
                listOf("Water"),
                "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/7.png",
                8.5f,
                6f,
                -1.9928572,
                -60.0552653
            )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Places.initialize(applicationContext, getString(R.string.API_KEY))

        setUpRecyclerView()
        floatingActionButton.setOnClickListener{
            val intent = Intent(this, ActivityAddPokemon::class.java)
            startActivityForResult(intent, ADD_POKEMON_REQUEST_CODE)
        }

        shouldDisplayEmptyView(pokemons.isEmpty())
    }

    private fun setUpRecyclerView() {
        rvPokemons.adapter = PokemonAdapter(pokemons) {
            val intent = Intent(this, PokemonDetailActivity::class.java).apply {
                putExtra(PokemonDetailActivity.POKEMON_EXTRA, it)
            }
            startActivity(intent)

        }
    }

    fun shouldDisplayEmptyView(isEmpty: Boolean){
        emptyView.visibility = if(isEmpty) View.VISIBLE else View.GONE

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == ADD_POKEMON_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            // todo : adiciona na lisa esse pokemon
        }
    }

    companion object {
        const val ADD_POKEMON_REQUEST_CODE = 1
    }
}